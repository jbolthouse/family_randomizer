from ast import arg
import json
import random
from sys import argv

GIFTS_DEPTH=1 # How many presents per person - TODO: Imp
JUSTKIDS=False # Parents give gifts to each other or just kids
LOCK_KIDS_ADULTS=False# Kids get kids and adults get adults - partially implemented

# First - let's define the families

#brauts = [("Jake", "k"), ("Annie", "k"), ("Simon", "k"), ("Nikky", "k"), ("Mike", "a"), ("Kristin", "a")]
brauts = [] # 2021 Brauts are opted out this year. 
bolts1 = [("Judah", "k"), ("Mark", "k"), ("Noelle", "k"), ("Caleb", "k")
, ("Brooke", "k"), ("John", "a"), ("Jess", "a")]
#adlers = [("Brett", "a"), ("Steph", "a"), ("Maddie", "k"), ("Bear", "k"), ("Susie", "k")]
adlers = [("Maddie", "k"), ("Bear", "k"), ("Susie", "k")] #2021 Adler adults are out this year. 
grpts = [("Mimi", "a"), ("Papa", "a")]
bolts2 = [("Ben", "a"), ("Olivia", "a")]

# Next let's build out the assignments taking into consideration the following rules:

# For each family, make sure that the people selected are not in their family
# Number of presents to purchase equivalent to number of kids... 1:1
# Brautigams can't draw gpts
# Gpts can't draw brauts





def g_reduce(p, famlist, gift_a) :
	a = ""
	i = 0
	while True:
		random.shuffle(famlist)
		a = famlist[0][0]
		if a not in [y for x,y in gift_a] and p != a:
			break
		i = i + 1
		if i == 10000 :
			break
	return a

def main():
	gift_a = []
	for p in brauts:
		if LOCK_KIDS_ADULTS:
			gift_a.append((p[0], g_reduce(p[0], list(filter( lambda x : x[1] == p[1] , bolts1 + adlers + bolts2)), gift_a  )))
		else:
			gift_a.append((p[0], g_reduce(p[0], bolts1 + adlers + bolts2, gift_a)))
	for p in grpts:
		if LOCK_KIDS_ADULTS:
			gift_a.append((p[0], g_reduce(p[0], list(filter( lambda x : x[1] == p[1] , bolts1 + adlers + bolts2)), gift_a)))
		else:
			gift_a.append((p[0], g_reduce(p[0], bolts1 + adlers + bolts2, gift_a)))
	for p in adlers:
		if LOCK_KIDS_ADULTS:
			gift_a.append((p[0], g_reduce(p[0], list(filter( lambda x : x[1] == p[1] , bolts1 + bolts2 + brauts + grpts)), gift_a)))
		else:
			gift_a.append((p[0], g_reduce(p[0], bolts1 + bolts2 + brauts + grpts, gift_a)))
	for p in bolts1:
		if LOCK_KIDS_ADULTS:
			gift_a.append((p[0], g_reduce(p[0], list(filter( lambda x : x[1] == p[1] , brauts + adlers + bolts2 + grpts)), gift_a)))
		else:
			gift_a.append((p[0], g_reduce(p[0], brauts + adlers + bolts2 + grpts, gift_a)))
	for p in bolts2:
		if LOCK_KIDS_ADULTS:
			gift_a.append((p[0], g_reduce(p[0], list(filter( lambda x : x[1] == p[1] , brauts + adlers + bolts1 + grpts)), gift_a)))
		else:
			gift_a.append((p[0], g_reduce(p[0], brauts + adlers + bolts1 + grpts, gift_a)))
	return gift_a
def naughty_nice(people):
	"""Ben -- at your request"""
	return list(map(lambda x: (x[0], ("Nice" if random.randint(0,1) == 1 else "Naughty") ) , people))
	


if len(argv) > 1:
	if argv[1] == "checkit2x":
		print(naughty_nice(main()))
else:
	print(main())
